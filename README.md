# nano-cli4j
A simple and small library to develop small CLI applications in Java.

## Coordinates
Maven artifact:
```xml
<dependency>
    <groupId>org.emerjoin</groupId>
    <artifactId>nano-cli4j</artifactId>
    <version>1.4.3-GA</version>
</dependency>
```
Maven Repository:
```xml
<repository>
    <id>emerjoin-oss</id>
    <name>central</name>
    <url>https://pkg.emerjoin.org/oss</url>
    <releases>
        <enabled>true</enabled>
    </releases>
</repository>
```

## Concepts
To be documented yet


## User Guide
To be documented yet