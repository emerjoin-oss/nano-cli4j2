package org.emerjoin.nanocli.parsing;

public class Token {

    private String value;
    private boolean quoted = false;

    Token(String value){
        this(value,false);
    }

    Token(String value, boolean quoted){
        this.value = value;
        this.quoted = quoted;
    }

    public boolean lengthEq(int length){
        return value.length() == length;
    }

    public boolean lengthGt(int length){
        return value.length() > length;
    }

    public boolean lengthLt(int length){
        return value.length() < length;
    }

    public boolean eq(char val){
        return value.equalsIgnoreCase(
                String.valueOf(
                val));
    }

    public boolean startsWith(String val){

        return value.startsWith(val);

    }

    public boolean startsWith(char val){

        return value.startsWith(String.valueOf(val));

    }

    public boolean eq(String val){
        return value.equalsIgnoreCase(
                val);
    }

    public boolean isQuoted() {
        return quoted;
    }

    public String content(){

        return this.value;

    }

    public String toString(){

        return this.content();

    }

}
