package org.emerjoin.nanocli.parsing;

class Buffer {

    private StringBuilder builder = new StringBuilder();
    private Token token = null;
    private boolean openQuote = false;
    private char lastChar = '?';

    public Token consumeToken(){
        Token copy = token;
        token = null;
        this.openQuote = false;
        return copy;
    }

    public boolean isEmpty(){

        return this.builder.length() == 0;

    }

    public Token drain(){

        return new Token(builder.toString());

    }

    public boolean hasToken(){

        return this.token!=null;

    }


    private void flagOpenQuote(char ch){
        if(ch=='"'&&!openQuote)
            this.openQuote = true;
    }

    public void put(char ch){
        try {
            if (builder.length() == 0) {
                builder.append(ch);
                return;
            }

            //Escaped double quote
            if (ch == '"' && lastChar == '\\') {
                builder.setCharAt(builder.length() - 1, ch);
                lastChar = ch;
                return;
            }

            //Token Completion
            if (ch == ' ' && !openQuote) {
                builder.append(ch);
                this.tokenComplete(false);
                return;
            }

            //Closure of quotes
            if (openQuote && ch == '"') {
                builder.append(ch);
                this.tokenComplete(true);
                this.openQuote = false;
                return;
            }

            this.lastChar = ch;
            builder.append(ch);
        }finally {
            flagOpenQuote(ch);
        }
    }

    private void tokenComplete(boolean quoted){
        this.snapshotToken(quoted);
        this.builder = new StringBuilder();
    }

    private void snapshotToken(boolean quoted){
        this.token = new Token(this.sanitize(builder.toString()),
                quoted);
    }

    private String sanitize(String token){
        String trimmed = token.trim();
        if(trimmed.startsWith("\"")&&trimmed.endsWith("\""))
            return trimmed.substring(1,trimmed.
                    length()-1);
        return trimmed;
    }

}
