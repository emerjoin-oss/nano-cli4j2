package org.emerjoin.nanocli.parsing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class TokenizedInput {

    private static final TokenizedInput EMPTY = new TokenizedInput(Collections.emptyList());

    private List<Token> tokenList;

    private TokenizedInput(List<Token> tokenList){
       this.tokenList = tokenList;
    }

    public static TokenizedInput empty(){
        return EMPTY;
    }

    public static TokenizedInput parse(String command){
        List<Token> tokenList = new ArrayList<>();
        Buffer buffer = new Buffer();
        for(char ch: command.toCharArray()){
            buffer.put(ch);
            if(buffer.hasToken()){
                tokenList.add(buffer.consumeToken());
            }
        }
        if(!buffer.isEmpty())
            tokenList.add(buffer.drain());
        return new TokenizedInput(tokenList);
    }

    public  int size(){

        return this.tokenList.size();

    }

    public Token lastToken(){

        return tokenList.get(tokenList.size()-1);

    }

    public Token firstToken(){

        return tokenList.get(0);

    }

    public Optional<Token> tokenAt(int position){
        if(position<0)
            throw new IllegalArgumentException("position ust be higher than -1");
        if(tokenList.size()>=position){
            return Optional.of(tokenList.get(position));
        }
        return Optional.empty();
    }

    public TokenizedInput slice(int position){
        if(position<0||position>=tokenList.size())
            throw new IllegalArgumentException("position should be higher than -1 and within the input size");
        return new TokenizedInput(this.tokenList.subList(position, tokenList
                .size()));
    }

    public TokenizedInput slice(int from, int to){
        if(to<=from)
            throw new IllegalArgumentException("to position can't be equals to from nor lesser");
        if(from<0||from>=tokenList.size())
            throw new IllegalArgumentException("from position should be within the input size range");
        if(to>=tokenList.size())
            throw new IllegalArgumentException("to position should be within the input size range");
        return new TokenizedInput(this.tokenList.subList(from,
                to+1));
    }


    public Cursor<Token> cursor(){

        return new Cursor<>(tokenList);

    }
}
