package org.emerjoin.nanocli.parsing;

import java.util.List;

public class Cursor<T> {

    private List<T> list;
    private int position = -1;

    public Cursor(List<T> list){
        this.list = list;
    }


    public boolean hasNext(){

        return !list.isEmpty() && position < (this.list.size() - 1);

    }

    public boolean hasPrevious(){

        return this.position > 0;

    }

    public void moveToFirst(){

        this.position = 0;

    }

    public void moveToLast(){

        this.position = list.size() -1;

    }

    public int position(){

        return this.position;

    }


    public T get(){

        return this.list.get(position);

    }

    public T next(){
        int target = position + 1;
        if(target >= list.size() )
            throw new IllegalStateException("There are no more elements to the right");
        T item = list.get(target);
        this.position = target;
        return item;
    }

    public T back(){
        int target = position - 1;
        if(target<0)
            throw new IllegalStateException("There no more elements to the left");
        T item = list.get(target);
        this.position = target;
        return item;
    }

}
