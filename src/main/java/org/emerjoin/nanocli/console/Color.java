package org.emerjoin.nanocli.console;

public enum Color {
    Default, Black, Red, Green, Yellow, Blue, Magenta, Cyan, White, Gray
}
