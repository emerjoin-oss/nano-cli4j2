package org.emerjoin.nanocli.console;

import com.github.tomaslanger.chalk.Chalk;

public class TextToken extends Token {

    private String text;
    private boolean bolded = false;

    TextToken(String text){
        this(text,false);
    }

    TextToken(String text, boolean bolded){
        this.text = text;
        this.bolded = bolded;
    }

    String getText() {
        return text;
    }

    boolean isBolded() {
        return bolded;
    }

    @Override
    String compile() {
        Chalk chalk = Chalk.on(text);
        if(isBolded())
            chalk.bold();
        return chalk.toString();
    }
}
