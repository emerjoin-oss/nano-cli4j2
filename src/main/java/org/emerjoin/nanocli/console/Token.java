package org.emerjoin.nanocli.console;

public abstract class Token {


    abstract String compile();

    public String toString(){
        return compile();
    }


}
