package org.emerjoin.nanocli.console;

public final class Text {

    private static final Token SPACE_TOKEN = new TextToken(" ");
    private static final Token NEW_LINE_TOKEN = new TextToken("\n");

    public static Token space(){
        return SPACE_TOKEN;
    }

    public static Token breakLine(){
        return NEW_LINE_TOKEN;
    }

    public static Token tab(int times){
        if(times<1)
            throw new IllegalArgumentException("times must be higher than zero");
        StringBuilder builder = new StringBuilder();
        for(int i=0; i < times; i++)
            builder.append("\t");
        return new TextToken(builder.toString());
    }

    public static Token plain(String text){
        if(text==null||text.isEmpty())
            throw new IllegalArgumentException("text must not be null nor empty");
        return new TextToken(
                text);
    }

    public static Token plain(String text, Color color){
        if(text==null||text.isEmpty())
            throw new IllegalArgumentException("text must not be null nor empty");
        if(color==null)
            throw new IllegalArgumentException("color must not be null");
        return new ColoredTextToken(text, false,
                color);
    }

    public static Token bolded(String text){
        if(text==null||text.isEmpty())
            throw new IllegalArgumentException("text must not be null nor empty");
        return new TextToken(text,
                true);
    }

    public static Token bolded(String text, Color color){
        if(text==null||text.isEmpty())
            throw new IllegalArgumentException("text must not be null nor empty");
        if(color==null)
            throw new IllegalArgumentException("color must not be null");
        return new ColoredTextToken(text, true,
                color);
    }

    public static TextSequence sequence(){

        return new TextSequence();

    }

}
