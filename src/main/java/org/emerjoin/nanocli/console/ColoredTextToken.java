package org.emerjoin.nanocli.console;

import com.github.tomaslanger.chalk.Chalk;

public class ColoredTextToken extends TextToken {

    private Color color;

    public ColoredTextToken(String text, boolean bolded, Color color){
        super(text,bolded);
        this.color = color;
    }

    Color getColor() {
        return color;
    }

    @Override
    String compile() {
        Chalk chalk = Chalk.on(getText());
        if(isBolded())
            chalk.bold();
        switch (color){
            case Red:
                chalk.red(); break;
            case Cyan:
                chalk.cyan(); break;
            case Blue:
                chalk.blue(); break;
            case Gray:
                chalk.gray(); break;
            case Green:
                chalk.green(); break;
            case Yellow:
                chalk.yellow(); break;
            case Magenta:
                chalk.magenta(); break;
        }
        return chalk.toString();
    }

}
