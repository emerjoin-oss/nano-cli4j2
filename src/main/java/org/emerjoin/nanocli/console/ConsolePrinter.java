package org.emerjoin.nanocli.console;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class ConsolePrinter {

    private PrintStream stream;

    public ConsolePrinter(PrintStream stream){
        if(stream==null)
            throw new IllegalArgumentException("stream must not be null");
        this.stream = stream;
    }

    public void println(Token... tokens){
        if(tokens==null||tokens.length==0)
            throw new IllegalArgumentException("at least one token is required");
        this.println(Arrays.asList(tokens));
    }

    public void print(Token... tokens){
        if(tokens==null||tokens.length==0)
            throw new IllegalArgumentException("at least one token is required");
        this.print(Arrays.asList(tokens));
    }

    public void print(List<Token> tokenList){
        StringBuilder builder = new StringBuilder();
        for(Token token: tokenList)
            builder.append(token);
        stream.print(builder.toString());
    }

    public void println(List<Token> tokenList){
        if(tokenList==null)
            throw new IllegalArgumentException("tokenList must not be null");
        StringBuilder builder = new StringBuilder();
        for(Token token: tokenList)
            builder.append(token);
        stream.println(builder.toString());
    }

}
