package org.emerjoin.nanocli.console;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class TextSequence {

    private Color color = Color.Default;
    private boolean bold = false;
    private List<Token> tokenList = new ArrayList<>();

    TextSequence(){

    }

    public TextSequence color(Color color){
        if(color==null)
            throw new IllegalArgumentException("color must not be null");
        this.color = color;
        return this;
    }

    public TextSequence defaultColor(){
        this.color = Color.Default;
        return this;
    }

    public TextSequence breakLine(){
        this.tokenList.add(Text.plain("\n"));
        return this;
    }

    public TextSequence space(){
        this.tokenList.add(Text.space());
        return this;
    }

    public TextSequence bold(){
        this.bold = true;
        return this;
    }

    public TextSequence plain(){
        this.bold = false;
        return this;
    }

    public TextSequence tab(){
        this.tokenList.add(Text.tab(1));
        return this;
    }

    public TextSequence appendIf(Supplier<Boolean> supplier, String value){
        if(supplier.get())
            this.append(value);
        return this;
    }

    public TextSequence append(String string){
        if(string==null)
            throw new IllegalArgumentException("string must not be null");
        this.tokenList.add(bold ? Text.bolded(string,color) : Text.plain(string,color));
        return this;
    }

    public List<Token> getTokenList() {
        return Collections.unmodifiableList(
                tokenList);
    }
}
