package org.emerjoin.nanocli.exception;

import org.emerjoin.nanocli.CommandApplicationException;

public class CommandInvocationException extends CommandApplicationException {

    public CommandInvocationException(String message){
        this(message,null);
    }

    public CommandInvocationException(String message, Throwable cause){
        super(message,cause);
    }

}
