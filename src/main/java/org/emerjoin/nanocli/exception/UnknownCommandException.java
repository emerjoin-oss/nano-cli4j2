package org.emerjoin.nanocli.exception;

public class UnknownCommandException extends CommandInvocationException {

    private String name;
    private String tag ="";

    public UnknownCommandException(String name) {
        super(String.format("Unknown command: %s",name));
        this.name = name;
    }

    public UnknownCommandException(String name, String tag) {
        super(String.format("Unknown command with name=%s and tag=%s",name,tag));
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getTag() {
        return tag;
    }
}
