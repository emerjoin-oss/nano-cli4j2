package org.emerjoin.nanocli.exception;


public class MalformedCommandInputException extends InvalidCommandInputException {

    public MalformedCommandInputException() {
        super("Command input is malformed");
    }

    public MalformedCommandInputException(String message) {
        super(message);
    }
}
