package org.emerjoin.nanocli.exception;

import java.util.Collections;
import java.util.List;

public class MissingArgumentsException extends InvalidCommandInputException {

    private List<String> nameList;

    public MissingArgumentsException(List<String> names) {
        super(String.format("Required arguments missing: %s",String.join(",",names)));
        this.nameList = names;
    }

    public List<String> getNameList() {
        return Collections.unmodifiableList(nameList);
    }

}
