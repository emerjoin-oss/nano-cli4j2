package org.emerjoin.nanocli.exception;

public class InvalidCommandInputException extends CommandInvocationException {

    public InvalidCommandInputException(String message) {
        super(message);
    }
}
