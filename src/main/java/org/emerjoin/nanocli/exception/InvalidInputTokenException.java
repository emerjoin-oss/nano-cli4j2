package org.emerjoin.nanocli.exception;

public class InvalidInputTokenException extends MalformedCommandInputException {

    public InvalidInputTokenException(String token){
        super(String.format("Invalid Input token: %s",token));
    }

}
