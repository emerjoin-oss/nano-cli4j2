package org.emerjoin.nanocli.exception;

public class InvalidCommandDefinitionException extends RuntimeException {

    private String name;

    public InvalidCommandDefinitionException(String name, String message){
        this(name,message,null);
    }

    public InvalidCommandDefinitionException(String name, String message, Throwable cause){
        super(String.format("Definition of command [%s] is not valid: %s",message),cause);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
