package org.emerjoin.nanocli.exception;

public class UnexpectedInputTokenException extends MalformedCommandInputException {

    public UnexpectedInputTokenException(String token){
        super(String.format("Unexpected token: %s",token));
    }

}
