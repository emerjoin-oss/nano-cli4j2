package org.emerjoin.nanocli.exception;

public class MissingArgumentException extends InvalidCommandInputException {

    private String name;

    public MissingArgumentException(String name) {
        super(String.format("Required argument missing: %s",name));
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
