package org.emerjoin.nanocli.exception;

public class UnknownParameterException extends InvalidCommandInputException {

    private String name;
    private char tag;

    public UnknownParameterException(char tag) {
        super(String.format("Unknown parameter with tag: %c",tag));
        this.tag = tag;
    }

    public UnknownParameterException(String name) {
        super(String.format("Unknown parameter with name: %s",name));
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public char getTag() {
        return tag;
    }
}
