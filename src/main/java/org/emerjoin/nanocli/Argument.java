package org.emerjoin.nanocli;

import java.util.*;
import java.util.function.Consumer;

public class Argument implements Comparable<Argument> {

    private String value;
    private List<String> valuesList = new ArrayList<>();
    private int position;

    public Argument(){

    }

    Argument(int position, String value){
        this.position = position;
        this.value = value;
    }

    Argument(int position, List<String> valuesList){
        this.position = position;
        this.valuesList = valuesList;
    }

    public void ifPresent(Consumer<String> consumer){
        if(consumer==null)
            throw new IllegalStateException("consumer must not be null nor empty");
        if(isPresent())
            consumer.accept(value);
    }

    public  boolean isPresent(){

        return value != null;

    }

    public List<String> valuesList(){
        return valuesList;
    }

    public Map<String,String> valuesMap(){
        Map<String,String> map = new HashMap<>();
        for(String value: valuesList){
            StringBuilder keyBuilder = new StringBuilder();
            int equalsCharIndex = 0;
            for(char ch: value.toCharArray()){
                if(ch!='='){
                    keyBuilder.append(ch);
                    equalsCharIndex++;
                }else{
                    map.put(keyBuilder.toString(),value.substring(
                            equalsCharIndex+1));
                }
            }
        }
        return Collections.unmodifiableMap(map);
    }

    public String value(){
        if(value==null)
            throw new IllegalStateException("value not present");
        return value;
    }

    public int compareTo(Argument obj){
        if(this.position < obj.position)
            return -1;
        else if(this.position > obj.position)
            return 1;
        return 0;
    }

}
