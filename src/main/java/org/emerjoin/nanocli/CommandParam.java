package org.emerjoin.nanocli;

import java.io.Serializable;

class CommandParam implements Serializable {

    private String name;
    private String description;
    private char tag;
    private boolean optional = false;
    private boolean repeatable = false;

    public CommandParam(char tag, String name, String description, boolean optional, boolean repeatable){
        this.tag = tag;
        this.name = name;
        this.description = description;
        this.optional = optional;
        this.repeatable = repeatable;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public char getTag() {
        return tag;
    }

    public boolean isOptional() {
        return optional;
    }

    public boolean isRepeatable() {
        return repeatable;
    }
}
