package org.emerjoin.nanocli;

import org.emerjoin.nanocli.console.Color;
import org.emerjoin.nanocli.console.Text;
import org.emerjoin.nanocli.exception.*;
import org.emerjoin.nanocli.meta.Cmd;
import org.emerjoin.nanocli.meta.Param;
import org.emerjoin.nanocli.parsing.Cursor;
import org.emerjoin.nanocli.parsing.Token;
import org.emerjoin.nanocli.parsing.TokenizedInput;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class CommandDefinition {

    private String name;
    private String subCommand;
    private String description;
    private CommandHandler handler;
    private Method executeMethod;
    private List<CommandParam> commandParamList = new ArrayList<>();

    public CommandDefinition(CommandHandler commandHandler){
        if(commandHandler==null)
            throw new IllegalArgumentException("commandHandler must not be null");
        this.handler = commandHandler;
        Class<? extends CommandHandler> commandType = commandHandler.getClass();
        Cmd annotation =  commandType.getAnnotation(Cmd.class);
        if(annotation==null)
            throw new IllegalArgumentException("@Cmd annotation missing on handler type: "+commandType.getCanonicalName());
        this.name = annotation.name();
        this.description = annotation.description();
        this.subCommand = annotation.subCommand();
        Optional<Method> optionalMethod = Arrays.stream(commandType.getMethods())
                .filter(it -> it.getName().equals("execute"))
                .findFirst();
        if(!optionalMethod.isPresent())
            throw new IllegalArgumentException("There is no execute(..) method on Handler type "+commandType.getCanonicalName());
        this.executeMethod = optionalMethod.get();
        java.lang.reflect.Parameter[] parameters = executeMethod.getParameters();
        for(java.lang.reflect.Parameter parameter: parameters){
            Param commandParamAnnotation = parameter.getAnnotation(Param.class);
            if(commandParamAnnotation ==null)
                throw new IllegalArgumentException(String.format("@Param annotation missing on parameter (%s)",
                        parameter.getName()));
            CommandParam metadata = new CommandParam(
                    commandParamAnnotation.tag(), commandParamAnnotation.name(),
                    commandParamAnnotation.description(),commandParamAnnotation.optional(),
                    commandParamAnnotation.repeatable());
            this.commandParamList.add(metadata);
        }
    }

    public String name(){
        return this.name;
    }

    public String getSubCommand() {
        return subCommand;
    }

    public String description(){
        return this.description;
    }

    public void executeCommand(TokenizedInput tokenizedInput){
        Argument[] arguments = getArguments(tokenizedInput);
        try {
            this.executeMethod.invoke(handler,arguments);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(String.format("execute() method of Handler %s is not public",handler.getClass().
                    getCanonicalName()));
        } catch (InvocationTargetException e) {
            Throwable exception = e.getTargetException();
            if(exception instanceof CommandInvocationException){
                throw (CommandInvocationException) exception;
            }
            throw new CommandInvocationException("unexpected error found during command execution",
                    exception);
        }
    }

    private boolean isSubCommand(){
        return !subCommand.isEmpty();
    }

    private Argument[] getArguments(TokenizedInput input){
        InvocationParameters parameters = new InvocationParameters(commandParamList);
        Cursor<Token> tokenCursor = input.cursor();
        if(isSubCommand()) {
            tokenCursor.next();
        }
        while (tokenCursor.hasNext()){
            Token token = tokenCursor.next();
            if(!token.isQuoted()&&token.startsWith('-')){
                InvocationParam param = identifyParameter(parameters,token);
                token = tokenCursor.next();
                param.supply(token.content());
            }else throw new UnexpectedInputTokenException(token.content());
        }
        List<String> missingParamsList = parameters.stream().filter(it -> it.isRequired() && !it.hasValue())
                .map(InvocationParam::name)
                .collect(Collectors.toList());
        if(!missingParamsList.isEmpty())
             throw new MissingArgumentsException(missingParamsList);
        return parameters.toArguments();
    }

    private InvocationParam identifyParameter(InvocationParameters parameters, Token token){
        InvocationParam param = null;
        Optional<InvocationParam> paramOptional;
        if(token.startsWith("--")) {
            if (token.lengthLt(3))
                throw new InvalidInputTokenException(token.content());
            String paramName = token.content().substring(2);
            paramOptional = parameters.named(paramName);
            if(!paramOptional.isPresent())
                throw new UnknownParameterException(paramName);
        }else {
            if (token.lengthLt(2)||token.lengthGt(2))
                throw new InvalidInputTokenException(token.content());
            char paramTag = token.content().substring(1).charAt(0);
            paramOptional = parameters.tagged(paramTag);
            if(!paramOptional.isPresent())
                throw new UnknownParameterException(paramTag);
        }
        return paramOptional.get();
    }

    public void printHelpSummary(Terminal terminal){
        if(subCommand.isEmpty()){
            terminal.println(Text.tab(1),
                    Text.plain(name, Color.Green), Text.space(), Text.plain("-"),Text.space(),
                    Text.plain(description));
        }else{
            terminal.println(Text.tab(1),
                    Text.plain(name, Color.Green), Text.space(), Text.plain(subCommand,Color.Cyan), Text.space(), Text.plain("-"),Text.space(),
                    Text.plain(description));
        }
    }

    public void printFullHelp(Terminal terminal){
        terminal.println(Text.sequence()
                .tab()
                .append("Purpose:")
                .space()
                .color(Color.Green)
                .append(description));
        if(!commandParamList.isEmpty())
            terminal.println(Text.sequence().tab().color(Color.Default)
                .append("Command Parameters:"));
        for(CommandParam commandParam: commandParamList){
            terminal.println(Text.sequence().tab().tab()
                    .color(Color.Green)
                    .append("[").append(commandParam.isOptional() ? "May": "Must").append("]")
                    .space()
                    .color(Color.Blue)
                    .appendIf(commandParam::isRepeatable,"[Repeatable] ")
                    .color(Color.Yellow)
                    .append(String.format("-%c --%s",commandParam.getTag(),commandParam.getName()))
                    .color(Color.Magenta)
                    .space()
                    .append("<value>")
                    .defaultColor()
                    .space()
                    .append(commandParam.getDescription()));
        }
    }


}
