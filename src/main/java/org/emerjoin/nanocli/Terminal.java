package org.emerjoin.nanocli;

import com.github.tomaslanger.chalk.Chalk;
import org.emerjoin.nanocli.console.*;

import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

public class Terminal {

    private ConsolePrinter printer;
    private Scanner scanner = new Scanner(System.in);

    private static Terminal INSTANCE;

    public static Terminal getDefaultInstance(){
        if(INSTANCE==null)
            INSTANCE = new Terminal();
        return INSTANCE;
    }


    private Terminal(){
        this.printer = new ConsolePrinter(System.out);
    }

    public Integer readInt(){

        return this.scanner.nextInt();

    }

    public Double readDouble(){

        return this.scanner.nextDouble();

    }

    public Long readLong(){

        return this.scanner.nextLong();

    }

    public String readString(){

        return this.scanner.nextLine();

    }


    public String readSecretString(){
        char[] chars = System.console().readPassword();
        return new String(chars);
    }

    public void print(Token token){
        this.printer.print(token);
    }

    public void print(Token... tokens){
        this.printer.print(tokens);
    }

    public void print(TextSequence textSequence){
        this.printer.print(textSequence.getTokenList());
    }

    public void println(TextSequence textSequence){
        if(textSequence==null)
            throw new IllegalArgumentException("textSequence must not be null");
        this.println(textSequence.getTokenList());
    }

    public void println(List<Token> tokenList){
        if(tokenList==null)
            throw new IllegalArgumentException("tokenList must not be null");
        this.printer.println(tokenList);
    }

    public void println(Token... tokens){
        this.printer.println(tokens);
    }


}
