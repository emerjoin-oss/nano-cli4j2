package org.emerjoin.nanocli;

import java.util.ArrayList;
import java.util.List;

class InvocationParam implements Comparable<InvocationParam> {

    private String name;
    private List<String> values = new ArrayList<>();
    private char tag;
    private int position;
    private String value;
    private boolean required;
    private boolean repeatable;

    InvocationParam(char tag, String name, boolean required, int position){
        this(tag,name,required,position,false);
    }

    InvocationParam(char tag, String name, boolean required, int position, boolean repeatable){
        this.tag = tag;
        this.name = name;
        this.required = required;
        this.position = position;
        this.repeatable = repeatable;
    }

    char tag(){
        return tag;
    }

    boolean isRequired(){
        return this.required;
    }

    String name(){
        return this.name;
    }

    void supply(String value){
        if(repeatable) {
            this.values.add(value);
        } else {
             this.value = value;
        }
    }

    Argument toArgument(){
        if(repeatable)
            return new Argument(position,values);
        else return new Argument(position,value);
    }

    boolean hasValue(){
        return value != null;
    }

    @Override
    public int compareTo(InvocationParam obj) {
        return Integer.compare(this.position, obj.position);
    }
}

