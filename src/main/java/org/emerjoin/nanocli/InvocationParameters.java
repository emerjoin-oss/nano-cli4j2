package org.emerjoin.nanocli;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InvocationParameters {

    private Map<Character,InvocationParam> paramMap = new HashMap<>();

    public Stream<InvocationParam> stream(){

        return this.paramMap.values().stream();

    }

    public InvocationParameters(List<CommandParam> paramList){
        int position = 0;
        for(CommandParam param: paramList){
            this.declared(param,position);
            position++;
        }
    }

    public void declared(CommandParam param, int position){
        if(param==null)
            throw new IllegalArgumentException("param must not be null");
        if(position<0)
            throw new IllegalArgumentException("position must not be less than zero");
        InvocationParam invocationParam = new InvocationParam(
                param.getTag(),param.getName(), !param.isOptional(),
                position,param.isRepeatable());
        this.paramMap.put(param.getTag(), invocationParam);
    }

    public Optional<InvocationParam> named(String name){
        return this.paramMap.values().stream()
                .filter(it -> it.name().equals(name))
                .findFirst();
    }

    public Optional<InvocationParam> tagged(char tag){
        return Optional.ofNullable(this.paramMap.get(
                tag));
    }

    public Argument[] toArguments(){
        List<Argument> sortedArgumentList = paramMap.values()
                .stream().sorted()
                .map(InvocationParam::toArgument).collect(Collectors.toList());
        Argument[] arguments = new Argument[sortedArgumentList.size()];
        sortedArgumentList.toArray(arguments);
        return arguments;
    }

}
