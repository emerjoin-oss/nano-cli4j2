package org.emerjoin.nanocli;

public class Context {

    private boolean verbose = false;
    private Terminal terminal = Terminal.getDefaultInstance();

    public Context(){

    }

    public Context(boolean verbose){
        this.verbose = verbose;
    }

    public Terminal terminal(){

        return terminal;

    }

    public CommandApplication app(){

        return CommandApplication.current();

    }

    public boolean isVerbose(){

        return this.verbose;

    }


}
