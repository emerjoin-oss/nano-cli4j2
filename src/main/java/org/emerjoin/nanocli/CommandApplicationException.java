package org.emerjoin.nanocli;

public class CommandApplicationException extends RuntimeException {

    public CommandApplicationException(String message){
        super(message);
    }

    public CommandApplicationException(String message, Throwable cause){
        super(message,cause);
    }

}
