package org.emerjoin.nanocli;

import org.emerjoin.nanocli.exception.CommandInvocationException;
import org.emerjoin.nanocli.exception.UnknownCommandException;
import org.emerjoin.nanocli.parsing.Token;
import org.emerjoin.nanocli.parsing.TokenizedInput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CommandApplication {

    private Context context;
    private List<CommandDefinition> commandDefinitionList = new ArrayList<>();

    private CommandApplication(Context context, List<CommandDefinition> definitions){
        this.context = context;
        this.commandDefinitionList.addAll(definitions);
    }
    private static final ThreadLocal<CommandApplication> THREAD_LOCAL_APPLICATION = new ThreadLocal<>();

    public static CommandApplication current(){
        CommandApplication application = THREAD_LOCAL_APPLICATION.get();
        if(application==null)
            throw new IllegalStateException("There is no active Command Application");
        return application;
    }

    public static final class Builder {

        private Context context = new Context();
        private List<CommandDefinition> definitionList = new ArrayList<>();

        private Builder(){

        }

        public Builder command(CommandHandler commandHandler){
            if(commandHandler==null)
                throw new IllegalArgumentException("commandHandler must not be null");
            commandHandler.setContext(context);
            this.definitionList.add(new CommandDefinition(commandHandler));
            return this;
        }

        public Builder context(Context context){
            if(context==null)
                throw new IllegalArgumentException("context must not be null");
            this.context = context;
            return this;
        }

        public Builder command(Class<? extends CommandHandler> type){
            if(type==null)
                throw new IllegalArgumentException("type must not be null");
            try {
                CommandHandler commandHandler =  type.newInstance();
                this.command(commandHandler);
            }catch (InstantiationException | IllegalStateException | IllegalAccessException ex){
                throw new CommandApplicationException("error instantiating handler type: "+type.getCanonicalName(),
                        ex);
            }
            return this;
        }

        public CommandApplication build(){
            if(definitionList.isEmpty())
                throw new IllegalStateException("add at least one Command");
            return new CommandApplication(context, definitionList);
        }

    }

    public static Builder builder(){

        return new Builder();

    }

    public void invokeCommand(String name, TokenizedInput input) throws CommandInvocationException {
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(input==null)
            throw new IllegalArgumentException("input must not be null");
        try{
            THREAD_LOCAL_APPLICATION.set(this);
            List<CommandDefinition> matchCommandsList = commandDefinitionList.stream()
                    .filter(it -> it.name().equals(name))
                    .collect(Collectors.toList());
            if(matchCommandsList.isEmpty())
                throw new UnknownCommandException(name);
            if(matchCommandsList.size()>1){
                Token tagToken = input.firstToken();
                String tag = tagToken.content();
                Optional<CommandDefinition> commandDefinitionOptional =
                        commandDefinitionList.stream().filter(it -> it.name()
                        .equals(name) && it.getSubCommand().equals(tag))
                        .findAny();
                if(!commandDefinitionOptional.isPresent())
                    throw new UnknownCommandException(name,tag);
                commandDefinitionOptional.ifPresent(command -> {
                    command.executeCommand(input);
                });
            }else {
                CommandDefinition commandDefinition = matchCommandsList.get(0);
                commandDefinition.executeCommand(input);
            }
        }finally {
            THREAD_LOCAL_APPLICATION.remove();
        }
    }

    public Context getContext(){
        return this.context;
    }

    public List<CommandDefinition> getCommandDefinitions(){

        return Collections.unmodifiableList(commandDefinitionList);

    }



}
