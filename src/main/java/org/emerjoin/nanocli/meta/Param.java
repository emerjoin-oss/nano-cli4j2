package org.emerjoin.nanocli.meta;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Param {
    char tag();
    String name();
    boolean repeatable() default false;
    boolean optional() default false;
    String description();
}
